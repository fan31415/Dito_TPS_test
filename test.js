var Web3 = require('web3');

/**
 * Global parametes
**/

//if show debug log
let isDebug = true;

//currently maxmum nodes
const limit = 4;

//the interval of every transaction be sent (ms)
const INTERVAL = 10;

const org_c_node = 'https://u0cij6uhtv:LLGWUpXLJG-LJdGlE0wfUkfwC1ht53lv6NB-PfOT8PQ@u0jl3gnpar-u0lzqfghx5-rpc.us-east-2.kaleido.io';
const org_b_node = 'https://u0orbaekq9:k9iuWUeQKrDURQbpw640C1ONhWtWkGIvTRKMfJjxFj4@u0jl3gnpar-u0l230ly8m-rpc.us-east-2.kaleido.io';
const org_b_node2 = 'https://u0orbaekq9:k9iuWUeQKrDURQbpw640C1ONhWtWkGIvTRKMfJjxFj4@u0jl3gnpar-u0dzcutm61-rpc.us-east-2.kaleido.io'
const org_b_node3 = 'https://u0orbaekq9:k9iuWUeQKrDURQbpw640C1ONhWtWkGIvTRKMfJjxFj4@u0jl3gnpar-u0d0o7hdfo-rpc.us-east-2.kaleido.io'
var web3 = new Web3(new Web3.providers.HttpProvider(org_c_node));
var web3_2 = new Web3(new Web3.providers.HttpProvider(org_b_node));
var web3_3 = new Web3(new Web3.providers.HttpProvider(org_b_node2));
var web3_4 = new Web3(new Web3.providers.HttpProvider(org_b_node3));
//var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));

//to check if network works right
// console.log(getTime());
web3.eth.getBlockNumber().then(console.log);
web3_2.eth.getBlockNumber().then(console.log);
web3_3.eth.getBlockNumber().then(console.log);
web3_4.eth.getBlockNumber().then(console.log);

//address of each node
let addr_node_c = '0xE4b77A9182f935d05b312E0A72AA52Ba58343D11';
let addr_node_b = '0x7389B261D189D0AbCAbFD019Cc89E65325987a5c';
let addr_node_b2 = '0x3B72d8C04CaeC68C9385bc680B2F1c472357CcF4';
let addr_node_b3 = '0x0e8d2377eFD540c19Efa1B07c91e73d177B6c952';
//address for test destination
let addr_test = '0x3B72d8C04CaeC68C9385bc680B2F1c472357CcF4';

var time = '0x' + getTime();
count = 0;



//pass parameter as node number
var args = process.argv.splice(2);

node_number = parseInt(args[0]);
//handle no parameter
if (isNaN(node_number)) {
    node_number = limit;
}
//handle illegal paramter
if (node_number > limit || node_number <= 0) {
    node_number = limit;
}
console.log('node number: ' + node_number);

interval = node_number * INTERVAL;
setInterval(() => {

    sendTransaction(web3, addr_node_c, addr_test);

    count++;
    if (node_number > 1) {
        setTimeout(() => {
            sendTransaction(web3_2, addr_node_b, addr_test);
        }, INTERVAL);
        count++;
    }

    if (node_number > 2) {
        setTimeout(() => {
            sendTransaction(web3_3, addr_node_b2, addr_test);
        }, INTERVAL*2);
        count++;
    }
    if (node_number > 3) {
        setTimeout(() => {
            sendTransaction(web3_4, addr_node_b3, addr_test);
        }, INTERVAL*3);
        count++;
    }


}, interval);

function getTime() {
    return Math.round(new Date().getTime());
}


function sendTransaction(node, addr1, addr2) {
    node.eth.sendTransaction({
        from: addr1,
        to: addr2,
    }, function(error, hash) {
        if (error == null) {
            console.log(hash);

        } else {
            if (isDebug) {
                console.error(error);
            }
        }

    }).catch((error) => {
        if (isDebug) {
            console.error(error);
        }
    });
}
