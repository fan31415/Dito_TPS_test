from web3 import Web3, HTTPProvider, IPCProvider, WebsocketProvider
from web3.middleware import geth_poa_middleware
import requests, json, time
import sys
import asyncio
import concurrent.futures
urls = []
web3s = []
apis = []
addrs = []
nodeNum = int(sys.argv[1])
maxCount = int(sys.argv[2])
addr_test = '0x3B72d8C04CaeC68C9385bc680B2F1c472357CcF4'
msg = "node number : {}"
print(msg.format(nodeNum))
msg = "max running round : {}"
print(msg.format(maxCount))




def sendTransaction(i):
    try:
        result = web3s[i].eth.sendTransaction({'to': addr_test, 'from': addrs[i]})
    except Exception as e:
        print(e)
    return Web3.toHex(result)
async def waitSend(i):
    print(i)
    result = await sendTransaction(i)
    print(result)


def setWeb3(i):
    web3 =  Web3(HTTPProvider(apis[i]))
    web3.middleware_stack.inject(geth_poa_middleware, layer=0)
    print(i)
    #print(web3.eth.blockNumber)
    web3s.append(web3)

sendingTask = []
def main():


    f = open("nodes.json", encoding='utf-8')  
    nodes = json.load(f)
    nodesB = nodes['nodesOrgB']
    # minimum is 2 node
    for node in nodesB[0:nodeNum-1]:
        urls.append(node['rpc'])
        addrs.append(node['C_ID'])
 
    header = "https://u0orbaekq9:k9iuWUeQKrDURQbpw640C1ONhWtWkGIvTRKMfJjxFj4@"
    for index in range(0, nodeNum-1):
        apis.append(header + urls[index][8:])
    #for org c
    apis.append('https://u0cij6uhtv:LLGWUpXLJG-LJdGlE0wfUkfwC1ht53lv6NB-PfOT8PQ@u0jl3gnpar-u0lzqfghx5-rpc.us-east-2.kaleido.io')
    addrs.append("0xE4b77A9182f935d05b312E0A72AA52Ba58343D11")
    

    time.sleep(1)

    print("start set web3")
    for i in range(0, nodeNum):
        setWeb3(i)



    print("start sendTransaction")

    
    with concurrent.futures.ProcessPoolExecutor(max_workers=50) as executor:
                futures = []
                for j in range(0, maxCount):
                    for i in range(0, nodeNum):
                        future = executor.submit(sendTransaction, i)
                        futures.append(future)
                        print(future)
                
                # for j in range(0, 1):
                #     futures.append([executor.submit(sendTransaction, i) for i in range(0, nodeNum)])
                for future in concurrent.futures.as_completed(futures):
                        print(future)
                        try: 
                            print(future.result())
                        except Exception as e:
                            print(e)

    # for j in range(0, 1):
    #     for i in range(0, nodeNum):
    #         sendingTask.append(waitSend(i))


main()
# loop = asyncio.get_event_loop()
# loop.run_until_complete(asyncio.wait(sendingTask))
# loop.close()
    
    


# addr_test = '0x3B72d8C04CaeC68C9385bc680B2F1c472357CcF4';
# maxCount = 1000;
# for count in range(0, maxCount):
#     for index in range(0, nodeNum):
#         print(index)
#         sendTransaction(index)
#         time.sleep(5);


    




