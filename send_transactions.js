var Web3 = require('web3');
var fs=require('fs');
/**
 * Global parametes
**/

//if show debug log
let isDebug = false;

//currently maxmum nodes
const limit = 50;

//the interval of every transaction be sent (ms)
let INTERVAL;

//pass parameter as node number
var args = process.argv.splice(2);


node_number = parseInt(args[0]);


//handle no parameter
if (isNaN(node_number)) {
    node_number = limit;
}
//handle illegal paramter
if (node_number > limit || node_number <= 0) {
    node_number = limit;
}
console.log('node number: ' + node_number);

INTERVAL = parseInt(args[1]);
//handle no parameter
if (isNaN(INTERVAL)) {
    INTERVAL = 50;
}
console.log('interval : ' + INTERVAL + " ms");


var file="nodes.json";

var header = "https://u0orbaekq9:k9iuWUeQKrDURQbpw640C1ONhWtWkGIvTRKMfJjxFj4@";

var result=JSON.parse(fs.readFileSync(file));

interval = node_number * INTERVAL;

let addr_test = '0x3B72d8C04CaeC68C9385bc680B2F1c472357CcF4';
let urls = [];
let addrs = [];
var web3 = [];

(async () => {
    //node_number-1 because only one org c
    for(var i = 0; i < node_number-1; i++) {
        urls[i] = header + result.nodesOrgB[i].rpc.slice(8);
        addrs[i] = result.nodesOrgB[i].C_ID;
        //console.log(urls[i]);
        //console.log(addrs[i]);
        web3[i] = await new Web3(new Web3.providers.HttpProvider(urls[i]));
    }
    for(var i = 0; i < result.nodesOrgB.length; i++) {
        //sendValuedTransaction(web3[5], addrs[5], addrs[i] );
    }
    
    //the final nodes is nodesc, hardcoded temporarily
urls[node_number-1] = 'https://u0cij6uhtv:LLGWUpXLJG-LJdGlE0wfUkfwC1ht53lv6NB-PfOT8PQ@u0jl3gnpar-u0lzqfghx5-rpc.us-east-2.kaleido.io';
web3[node_number-1] = new Web3(new Web3.providers.HttpProvider(urls[node_number-1]));
addrs[node_number-1] = "0xE4b77A9182f935d05b312E0A72AA52Ba58343D11";

setInterval(() => {
    console.log(web3.length);
    web3.forEach( function(web, index) {
        setTimeout(() => {
            //console.log(index);
            sendTransaction(web, addrs[index], addr_test);
        }, INTERVAL*index);
    });
}, interval);
})();
function sendValuedTransaction(node, addr1, addr2) {
    node.eth.sendTransaction({
        from: addr1,
        to: addr2,
        value: '1000000000000000000000',
    }, function(error, hash) {
        if (error == null) {
            console.log(hash);

        } else {
            if (isDebug) {
                console.error(error);
            }
        }
    }).catch((error) => {
        if (isDebug) {
            console.error(error);
        }
    });
}
function sendTransaction(node, addr1, addr2) {
    node.eth.sendTransaction({
        from: addr1,
        to: addr2,
    }, function(error, hash) {
        if (error == null) {
            //console.log(hash);
        } else {
            if (isDebug) {
                console.error("$///////");
                console.error(addr1);
                console.error(error);
                
            }
        }

    }).catch((error) => {
        if (isDebug) {
            console.error(error);
        }
    });
}

