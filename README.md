# Test for Kaleido TPS

## Requirement
* node.js
* yarn/npm
* python3

## Setting Up Environment
`yarn install`

`pip install web3`

## Running Test (js)
### Test for 4 node with 20ms interval
`node send_transactions.js 4 20`

`python get_tps.py`

### Test for 50 nodes with 10ms interval

`node send_transactions.js 50 10`

`python get_tps.py`

## Running Test (python)
* not fully tested yet due to some temporary network problems

### Test for 5 nodes with 1 round
`python send_transaction.py 5 1`

`python get_tps.py`
### Test for 50 nodes with 20 rounds
`python send_transaction.py 50 20`

`python get_tps.py`
## Notice
* change `isDebug` in global parameters is `send_transactions.js` to show/off the `rate limit error` (In demo presentation I prefer close this error log`
* You will find the transaction send speed is fast at first but slower then. The reason is that at first several seconds there will not receive `rate limit error`but then will, which can be attributed to the API limit of Kaleido.
* `get_tps.py` is designed to measure TPS of Kaleido in 30s currently, it would be better to run it after several seconds we run the `send_transactions.js` to get a more stable result. Besides this python script can be slow for the process of receiving blocks infomation from Kaleido is so slow. As a result, this script usually will not ended within 30 seconds, and maybe failed because of Kaleido internal error. So I recommend to run `get_tps.py` within more than one ternimal at the same time to avoid failure.


